import React from "react";
import ServerContextProvider from "./context/ServerContextProvider";
import AppRouter from "./routers/AppRouter";
import "./styles/styles.scss";
const App = () => {
	return (
		<ServerContextProvider>
			<AppRouter />
		</ServerContextProvider>
	);
};

export default App;
