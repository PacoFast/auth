import React from 'react';

const ErrorComponent = ({message}) => {
	return(
		<div className="error">
			<span>{message}</span>
		</div>
	)
};

export default ErrorComponent;
