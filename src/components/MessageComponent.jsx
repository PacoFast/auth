import React from 'react'

const Message = ({message}) => {
	return (
		<div className='message-container'>
			<span>{message}</span>
		</div>
	)
}

export default Message;
