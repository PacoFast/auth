import { useContext, useRef } from "react";
import {ServerContext} from "../context/ServerContextProvider";

/*
@params img is the user img
*/
const UserImgComponent = ({ img, isSmall, setIsVisible, isEdit }) => {

	const fileRef = useRef(null);
	const imgRef = useRef(null);

	const { fileUpload } = useContext(ServerContext);

	const handleFile = ({target}) => {
		const file = target.files[0];

		previewImg(file);
		uploadImg(file);
	};

	const previewImg = (file) => {
		//just previewing a img
		const src = URL.createObjectURL(file);
		imgRef.current.src = src;
	}

	const uploadImg = (file) => {
		fileUpload(file).then( console.log ).catch(console.warn);
	}

	return (
		<>
			{isEdit === true ? (
				<div
					onClick={() => fileRef.current.click()}
					className={`
					${
						isSmall === false
							? "img-container"
							: "img-container img-small-container pointer"
					} edit-photo
				`}
				>
					<img className="user-img" src={img} alt="userimg" ref={imgRef} />
					<input onChange={handleFile}  type="file" ref={fileRef}/>
				</div>
			) : (
				<div
					className={`
					${
						isSmall === false
							? "img-container"
							: "img-container img-small-container pointer"
					}
				`}
				>
					<img className="user-img" src={img} alt="userimg" />
				</div>
			)}
		</>
	);
};

export default UserImgComponent;
