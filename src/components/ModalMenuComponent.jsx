import React, {useContext} from "react";
import {ServerContext} from "../context/ServerContextProvider";
import {setLocalStorage} from "../helpers/localStorageUtility";
import auth from "../types/auth";
const ModalMenuComponent = ({ isVisible }) => {


	const {authDispatch} = useContext(ServerContext);

	const handleLogout = () => {
		authDispatch({
			type: auth.logout,
		});
		setLocalStorage('token', '');
	};

	return (
		<ul
			className={`modal-menu ${isVisible === true && 'modal-menu--active'}`}
		>
			<li className="modal-menu__item profile">
				<span>My Profile</span>
			</li>
			<li className="modal-menu__item group-chat">
				<span>Group Chat</span>
			</li>
			<li className="modal-menu__item logout" onClick={handleLogout}>
				<span>Logout</span>
			</li>
		</ul>
	);
};

export default ModalMenuComponent;
