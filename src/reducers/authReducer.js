import auth from "../types/auth";

const authReducer = (initialState, action) => {
	switch(action.type){
		case auth.login:
			return {
				isLogged: true,
			}
		case auth.logout:
			return {
				isLogged: false,
			}
		default: return initialState;
	};
};

export default authReducer;