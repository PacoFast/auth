import userTypes from "../types/user";

export const userReducer = (initialState, action) => {
	switch(action.type){
		case userTypes.get:
			return {
				...action.payload
			}
		default: return initialState;
	}
};