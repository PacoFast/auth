// import Facebook from "../assets/Facebook.svg";
// import Google from "../assets/Google.svg";
// import Github from "../assets/Github.svg";
// import Twitter from "../assets/Twitter.svg";

import React, { useState, useContext, useRef, useEffect } from "react";
import { ServerContext } from "../context/ServerContextProvider";
import { setLocalStorage } from "../helpers/localStorageUtility";

import auth from "../types/auth";
import ErrorComponent from "../components/ErrorComponent";
import { useNavigate } from "react-router-dom";
const PageLogin = () => {
	const { authDispatch, login } = useContext(ServerContext);

	const isMounted = useRef(true);

	const navigate = useNavigate();

	const [formValues, setFormValues] = useState({
		email: "",
		password: "",
	});

	const [isError, setIsError] = useState(false);

	const { email, password } = formValues;

	const handleInputChange = ({ target }) => {
		setFormValues({
			...formValues,
			[target.name]: target.value,
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		if (password.trim().length < 4 || email.trim().length < 4) return;

		login(email, password)
			.then((token) => {
				console.log(token);
				if (token === undefined) return displayError();
				setLocalStorage("token", token);
				authDispatch({
					type: auth.login,
				});

				setIsError(false);
			})
			.catch((err) => console.log(err));
	};

	const displayError = () => {
		setIsError(true);
		setTimeout(() => {
			if (isMounted.current === true) setIsError(false);
		}, 3000);
	};

	useEffect(() => {
		return () => {
			isMounted.current = false;
		};
	}, []);

	return (
		<div className="auth-container">
			<div className="auth-header">
				<h2 className="header__title lh-25">Login</h2>
				{isError && <ErrorComponent message="user not auth" />}
			</div>
			<form className="form" onSubmit={handleSubmit}>
				<div className="form__field">
					<input
						type="email"
						placeholder="Email"
						name="email"
						value={email}
						onChange={(e) => handleInputChange(e)}
					/>
				</div>
				<div className="form__field">
					<input
						type="password"
						placeholder="Password"
						name="password"
						value={password}
						onChange={(e) => handleInputChange(e)}
					/>
				</div>
				<div className="form__field">
					<input
						className="button button-blue"
						type="submit"
						value="Login"
					/>
				</div>
				<p className="fs-small txt-muted mb-2">
					Not registered yet?{" "}
					<span
						className="link"
						onClick={() => {
							navigate("/auth/register");
						}}
					>
						Register
					</span>
				</p>
				<div className="auth-buttons">
					{/* <img
						src={Facebook}
						alt="login with facebook"
						className="pointer"
					/>
					<img
						src={Twitter}
						alt="login with twitter"
						className="pointer"
					/>
					<img
						src={Github}
						alt="login with github"
						className="pointer"
					/>
					<img
						src={Google}
						alt="login with google"
						className="pointer"
					/> */}
				</div>
			</form>
		</div>
	);
};

export default PageLogin;
