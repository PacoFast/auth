import React, { useState, useContext, useEffect, useRef } from "react";
// import Facebook from "../assets/Facebook.svg";
// import Google from "../assets/Google.svg";
// import Github from "../assets/Github.svg";
// import Twitter from "../assets/Twitter.svg";
import {ServerContext} from '../context/ServerContextProvider';
import {useNavigate} from "react-router-dom";
// import Message from "../components/MessageComponent";
import ErrorComponent from "../components/ErrorComponent";
const PageRegister = () => {
	const [formValues, setFormValues] = useState({
		email: "",
		password: "",
	});

	const [isError, setIsError] = useState(false);

	const isMounted = useRef(true);

	const navigate = useNavigate();

	const {register} = useContext(ServerContext);

	const { email, password } = formValues;

	const handleInputChange = ({ target }) => {
		setFormValues({
			...formValues,
			[target.name]: target.value,
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		if( password.trim().length < 4 || email.trim().length < 4 ) {
			return;
		}

		register(email, password)
			.then(({msg, status}) => {
				if(status === true ) {
					navigate('/auth/login', {
						replace: true,
					});
				} else {
					setIsError(true)
					setTimeout(() => {
						if( isMounted.current === true ) {
							setIsError(false);
						}
					}, 3000);
				}
			}).catch(err => console.warn(err));

	};

	useEffect( () => {
		return () => isMounted.current = false;
	}, [])

	return (
		<div className="auth-container">
			<div className="auth-header">
				<h2 className="header__title lh-25">
					Join thousands of learners from around the world
				</h2>
				<p className="header__text lh-22">
					Master web development by making real-life projects. There
					are multiple partners for you choose
				</p>
				{isError &&
					<ErrorComponent message="The user alredy exists"/>
				}
			</div>
			<form className="form" onSubmit={handleSubmit}>
				<div className="form__field">
					<input
						type="email"
						placeholder="Email"
						name="email"
						value={email}
						onChange={handleInputChange}
					/>
				</div>
				<div className="form__field">
					<input
						type="password"
						placeholder="Password"
						name="password"
						value={password}
						onChange={handleInputChange}
					/>
				</div>
				<div className="form__field">
					<input
						className="button button-blue"
						type="submit"
						value="Start coding now"
					/>
				</div>
				<p className="fs-small txt-muted mb-2">
					Or continue with these social media profile
				</p>
				<span className="fs-small txt-muted mb-1">
					Are alredy member?<span className="link" onClick={() => navigate('/auth/login')}>Login</span>
				</span>
				<div className="auth-buttons">
					{/* <img
						src={Facebook}
						alt="login with facebook"
						className="pointer"
					/>
					<img
						src={Twitter}
						alt="login with twitter"
						className="pointer"
					/>
					<img
						src={Github}
						alt="login with github"
						className="pointer"
					/>
					<img
						src={Google}
						alt="login with google"
						className="pointer"
					/> */}
				</div>
			</form>
		</div>
	);
};

export default PageRegister;
