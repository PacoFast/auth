import React, { useContext, useEffect, useRef, useState } from "react";

import user from "../assets/user.jpg";
import icon from "../assets/icon.svg";
import ModalMenuComponent from "../components/ModalMenuComponent";
import UserImgComponent from "../components/UserImgComponent";
import { ServerContext } from "../context/ServerContextProvider";
// import { getLocalStorage } from "../helpers/localStorageUtility";
// import { getSuggestedQuery } from "@testing-library/react";
import Message from "../components/MessageComponent";
import { useNavigate } from "react-router-dom";
const PagePersonal = () => {
	const { updateUser, userState } = useContext(ServerContext);

	const [isVisible, setIsVisible] = useState(false);

	const navigate = useNavigate();

	const [isMessageVisible, setIsMessageVisible] = useState(false);

	const [formValues, setFormValues] = useState({
		name: userState.name,
		bio: userState.bio,
		phone: userState.phone,
		email: userState.email,
		password: "",
	});

	const { name, bio, phone, email, password } = formValues;

	const isMounted = useRef(true);

	const handleSubmit = (e) => {
		e.preventDefault();

		updateUser(formValues).then((data) => {
			const { status } = data;
			if (status === true) {
				setIsMessageVisible(true);

				navigate("/personal");
			}
		});
	};

	useEffect(() => {
		return () => {
			isMounted.current = false;
		};
	}, []);

	const handleInputChange = ({ target }) => {
		setFormValues({
			...formValues,
			[target.name]: target.value,
		});
	};

	return (
		<>
			<header className="header">
				<img src={icon} alt="icono-empresa" />
				<div
					onClick={() => setIsVisible(!isVisible)}
				>
					<UserImgComponent
						img={userState.photoURL !== "" ? userState.photoURL : user}
						isSmall={true}
						setIsVisible={setIsVisible}
					/>
				</div>
				<ModalMenuComponent isVisible={isVisible} />
			</header>
			<main className="edit-container">
				<form onSubmit={handleSubmit} className="edit-container__form">
					<fieldset>
						<div className="edit-container__header">
							<div className="edit-container__text">
								<legend className="edit-container__form-legend">
									Change Info
								</legend>
								<span className="txt-muted fs-small">
									Changes will be reflected for every services
								</span>
							</div>
							<div className="edit-container__img-container">
								<UserImgComponent
									img={
										userState.photoURL !== ""
											? userState.photoURL
											: user
									}
									isSmall={false}
									isEdit={true}
									setIsVisible={setIsVisible}
								/>
								<span className="txt-muted fs-small">
									change photo
								</span>
							</div>
							{isMessageVisible && (
								<Message message="The user has been updated" />
							)}
						</div>
						<div className="edit-container__form-field">
							<label className="edit-container__form-label">
								Name
							</label>
							<input
								type="text"
								className="edit-container__input"
								placeholder="Enter your name"
								name="name"
								value={name}
								onChange={handleInputChange}
							/>
						</div>
						<div className="edit-container__form-field">
							<label className="edit-container__form-label">
								Bio
							</label>
							<textarea
								className="edit-container__textarea"
								placeholder="Enter your bio..."
								name="bio"
								value={bio}
								onChange={handleInputChange}
							></textarea>
						</div>
						<div className="edit-container__form-field">
							<label className="edit-container__form-label">
								Phone
							</label>
							<input
								type="text"
								className="edit-container__input"
								placeholder="Enter your phone"
								name="phone"
								value={phone}
								onChange={handleInputChange}
							/>
						</div>
						<div className="edit-container__form-field">
							<label className="edit-container__form-label">
								Email
							</label>
							<input
								type="text"
								className="edit-container__input"
								placeholder="Enter your email"
								name="email"
								value={email}
								onChange={handleInputChange}
							/>
						</div>
						<div className="edit-container__form-field">
							<label className="edit-container__form-label">
								Password
							</label>
							<input
								type="text"
								className="edit-container__input"
								placeholder="Enter your new password"
								name="password"
								value={password}
								onChange={handleInputChange}
							/>
						</div>
						<input
							type="submit"
							value="Save"
							className="button button-blue edit-container__form-submit"
						/>
					</fieldset>
				</form>
			</main>
		</>
	);
};

export default PagePersonal;
