import React, { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";

import user from "../assets/user.jpg";
import icon from "../assets/icon.svg";
import ModalMenuComponent from "../components/ModalMenuComponent";
import UserImgComponent from "../components/UserImgComponent";
import { getLocalStorage } from "../helpers/localStorageUtility";
import { ServerContext } from "../context/ServerContextProvider";
import userTypes from "../types/user";

const PagePersonal = () => {
	const { getUser, userDispatch, userState } = useContext(ServerContext);
	const [isVisible, setIsVisible] = useState(false);

	const {
		name = "",
		email = "",
		bio = "",
		photoURL = "",
		phone = "",
	} = userState;

	const navigate = useNavigate();

	useEffect(() => {
		const token = getLocalStorage("token");

		getUser(token)
			.then(({ user }) => {
				userDispatch({
					type: userTypes.get,
					payload: user,
				});
			})
			.catch((err) => console.warn(err));
	}, []);

	return (
		<>
			<header className="header">
				<img src={icon} alt="icono-empresa" />
				<div
					className="header__img-container pointer"
					onClick={() => setIsVisible(!isVisible)}
				>
					<UserImgComponent
						img={photoURL === "" ? user : photoURL}
						isSmall={true}
						setIsVisible={setIsVisible}
					/>
				</div>
				<ModalMenuComponent isVisible={isVisible} />
			</header>
			<main className="info-container">
				<div className="top-text">
					<h1 className="center-txt fs-medium m-0">Personal Info</h1>
					<span className="center-txt block fs-small txt-muted">
						Basic info, like your name and photo
					</span>
				</div>
				<ul className="list">
					<header className="info-container__header">
						<div className="header__text">
							<h3 className="fs-light-medium m-0">Profile</h3>
							<span className="txt-muted fs-small">
								Some info may be visible to other people
							</span>
						</div>
						<button
							className="button edit-button"
							onClick={() => navigate("/edit")}
						>
							Edit
						</button>
					</header>
					<li className="list__item">
						<span className="left txt-muted">photo</span>
						<div className="right right-img">
							<UserImgComponent
								img={photoURL === "" ? user : photoURL}
								isEdit={false}
								isSmall={false}
							/>
						</div>
					</li>
					<li className="list__item">
						<span className="left txt-muted">name</span>
						<span className="right">{name}</span>
					</li>
					<li className="list__item">
						<span className="left txt-muted">bio</span>
						<span className="right">{bio}</span>
					</li>
					<li className="list__item">
						<span className="left txt-muted">phone</span>
						<span className="right">{phone}</span>
					</li>
					<li className="list__item">
						<span className="left txt-muted">email</span>
						<span className="right">{email}</span>
					</li>
					<li className="list__item">
						<span className="left txt-muted">password</span>
						<span className="right">****************</span>
					</li>
				</ul>
			</main>
		</>
	);
};

export default PagePersonal;
