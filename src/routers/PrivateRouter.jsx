import { Navigate } from "react-router-dom";

const PrivateRouter = ({children, isLogged}) => {
	return ( isLogged === true ) ? children : <Navigate to="/auth/login"/>
}
 
export default PrivateRouter;