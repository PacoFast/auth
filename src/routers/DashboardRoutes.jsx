import { Routes, Route, Navigate } from "react-router-dom";
import PageEditInfo from "../pages/PageEditInfo";
import PagePersonal from "../pages/PagePersonal";

const DashboardRoutes = () => {
	return ( 
		<>
			<Routes>
				<Route path="personal" element={<PagePersonal/>}/>
				<Route path="edit" element={<PageEditInfo/>}/>
				<Route path="*" element={<Navigate to="/personal"/>}/>
			</Routes>
		</>
	);
}
 
export default DashboardRoutes;