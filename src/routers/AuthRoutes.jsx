import { Routes, Route, Navigate } from "react-router-dom";
import PageLogin from "../pages/PageLogin";
import PageRegister from "../pages/PageRegister";

const AuthRoutes = () => {
	return ( 
		<Routes>
			<Route path="login" element={<PageLogin/>}/>
			<Route path="register" element={<PageRegister/>}/>
			<Route path="*" element={<Navigate to="/auth/login"/>}/>
		</Routes>
	 );
}
 
export default AuthRoutes;