import React, {useContext} from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import {ServerContext} from '../context/ServerContextProvider';
import AuthRoutes from './AuthRoutes';
import DashboardRoutes from './DashboardRoutes';
import PrivateRouter from './PrivateRouter';
import PublicRouter from './PublicRouter';
const AppRouter = () => {

	const {authState} = useContext(ServerContext);
	const {isLogged} = authState;

	console.log(isLogged);

	return (
		<BrowserRouter>
			<Routes>
				<Route exact path="/auth/*" element={
					<PublicRouter isLogged={isLogged}>
						<AuthRoutes />
					</PublicRouter>
				}>
				</Route>
				<Route path="/*" element={
					<PrivateRouter isLogged={isLogged}>
						<DashboardRoutes />
					</PrivateRouter>
				}>
				</Route>
			</Routes>
		</BrowserRouter>
	);
}

export default AppRouter;