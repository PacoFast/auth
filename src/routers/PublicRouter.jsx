import { Navigate } from "react-router-dom";

const PublicRouter = ({children, isLogged}) => {
	return ( isLogged === true ) ? <Navigate to="/"/> : children;
}
 
export default PublicRouter;