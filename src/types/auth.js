const auth = {
	login: "[auth] login",
	logout: "[auth] logout",
};

export default auth;