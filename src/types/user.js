const userTypes = {
	get: "[user] get user",
	update: "[user] update user",
};

export default userTypes;