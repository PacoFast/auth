import { createContext, useEffect, useReducer } from "react";

import { getValues, postFetch, putImg, putSession, putValues } from "./fetchUtility";
import authReducer from "../reducers/authReducer";
import { getLocalStorage} from "../helpers/localStorageUtility";
import { userReducer } from "../reducers/userReducer";
import auth from "../types/auth";

export const ServerContext = createContext();

const ServerContextProvider = ({children}) => {

	const URL = process.env.REACT_SERVER_URL || 'http://localhost:8080';

	const init = () => {
		return getLocalStorage('token') !== null ? {isLogged: true} : {isLogged:false}
	}
	
	const [authState, authDispatch] = useReducer(authReducer, {}, init);
	const [userState, userDispatch] = useReducer( userReducer, {});

	useEffect( () => {
		stillLogged().then(isLogged => {
			console.log('está logeado: ', isLogged);
			if(isLogged === false ){
				authDispatch({
					type: auth.logout,
				})
			} else {
				authDispatch({
					type: auth.login,
				})
			}
		}).catch(err => console.log(err));
	}, []);

	const stillLogged = async () => {
		return new Promise((resolve, reject) => {
			putSession( URL + '/auth/still_logged').then( (logged) => {

				if(logged === true) {
					resolve(true);
				} else {
					resolve(false);
				}

			}).catch((err) => reject(err));
		})
	};

	const login = (email, password) => {
		const bodyData = {
			email, 
			password,
		}
		return new Promise( (resolve, reject) =>  {
			postFetch( URL + '/auth/login', bodyData ).then( data => {
				const {token} = data;
				resolve(token);
			} )
			.catch( err => {
				reject(err);
			} );
		} );
	};


	const register = ( email, password ) => {
		const bodyData = { email, password }
		return new Promise( (resolve, reject) => {
			postFetch( URL + '/auth/register', bodyData ).then( data => {
				resolve(data);
			} ).catch( err => reject(err) );
		});  
	};

	const updateUser = ( values = {} ) => {
		return new Promise( (resolve, reject) => {
			putValues(URL + '/user/edit',values).then( data => {
				resolve(data);
			} ).catch( err => reject(err));
		});
	}

	const getUser = ( token ) => {
		return new Promise( (resolve, reject) => {
			getValues(`${URL}/user/user_info`, token).then( data => {
				resolve(data);
			} ).catch(err => reject(err));
		});
	};

	const fileUpload = ( file ) => {
		const data = new FormData();
		data.append('img', file );
		return new Promise( (resolve, reject) => {
			putImg( URL + '/user/img_upload', data ).then( data => {
				resolve(data);
			} ).catch( err => reject(err) );
		} );
	};

	return (
		<ServerContext.Provider value={{
			authState,
			authDispatch,
			userState,
			userDispatch,

			fileUpload,
			login, register,
			stillLogged,
			getUser, updateUser,
		}}>
			{children}
		</ServerContext.Provider>
	);
}

export default ServerContextProvider;