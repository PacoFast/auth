import { getLocalStorage } from "../helpers/localStorageUtility";

const postFetch = (url, bodyData) => {
	const fetchOptions = {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify(bodyData),
	};

	return new Promise((resolve, reject) => {
		fetch(url, fetchOptions)
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				resolve(data);
			})
			.catch((err) => reject(err));
	});
};
//uplaodimg
const putImg = (url, bodyData) => {
	const token = getLocalStorage("token");

	const fetchOptions = {
		method: "PUT",
		headers: {
			token: token,
		},
		body: bodyData,
	};

	return new Promise((resolve, reject) => {
		fetch(url, fetchOptions)
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				resolve(data);
			})
			.catch((err) => reject(err));
	});
};


const putSession = (url) => {

	const token = getLocalStorage('token');

	console.log('token', token);

	const fetchOptions = {
		method: 'GET', 
		headers: {
			token: token,
		},
	};

	return new Promise( (resolve, reject) => {
		fetch( url, fetchOptions ).then(data => data.json())
		    .then( res => {
				  const {logged} = res;
				  console.log(logged);
				  if(logged === true) {
					  return resolve(true);
				  }
				  resolve(false);
			  } )
			.catch(err => reject(err));
	});
};


const putValues = (url, bodyData) => {
	const token = getLocalStorage("token");

	const fetchOptions = {
		method: "PUT",
		headers: {
			"Content-Type": "application/json",
			token: token,
		},
		body: JSON.stringify(bodyData),
	};
	return new Promise((resolve, reject) => {
		fetch(url, fetchOptions)
			.then((res) => res.json())
			.then((data) => {
				resolve(data);
			})
			.catch((err) => reject(err));
	});
};

const getValues = (url, token) => {
	const fetchOptions = {
		method: "GET",
		headers: {
			'token': token,
		},
	};
	return new Promise((resolve, reject) => {
		fetch(url, fetchOptions)
			.then((res) => res.json())
			.then((data) => {
				resolve(data);
			})
			.catch((err) => reject(err));
	});
};

export { getValues, postFetch, putImg, putValues, putSession };
