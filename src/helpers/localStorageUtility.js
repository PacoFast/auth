const getLocalStorage = (itemKey) => {
	return localStorage.getItem(itemKey) || null;
};

const setLocalStorage = ( itemKeyName, itemValue ) => {
	localStorage.setItem(itemKeyName, itemValue);
}

export {
	setLocalStorage,
	getLocalStorage,
}
