export const facebookLogin = () => {
	FB.init({
		appId: null,
		cookie: true,
		xfbml: true,
		version: `{api_version}`,
	});

	FB.AppEvents.logPageView();
};

facebookLogin();
